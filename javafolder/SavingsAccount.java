public class SavingsAccount {
	private String name;
	private double accountbal;
	public int accountid;
	public static int counter = 1000;

	public SavingsAccount(String name) {
		this.name = name;
		this.accountid = counter;
		counter++;
	}

	public double deposit(int amt) {
		accountbal = accountbal + amt;
		// System.out.println("THE DEPOSITED AMOUNT IS "+accountbal);

		return accountbal;
	}

	public double withdraw(int amt) {
		if (amt > accountbal) {
			System.out.println("insufficient balance");
		} else {
			accountbal = accountbal - amt;
		}
		return amt;

	}

	public double displaybal() {
		System.out.println("THE BALANCE IS " + accountbal);
		return accountbal;

	}

	public String getName() {
		return name;
	}

}
