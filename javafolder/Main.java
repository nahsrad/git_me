public class Main{
    public static void main(String[] args){

        BankAccount2 obj1=new SavingsAccount2();
        obj1.deposit(50000);
        obj1.withdraw(20000);
        BankAccount2 obj2=new CurrentAccount();
        obj2.deposit(20000);
        obj2.withdraw(10000);
        BankAccount2 obj3=new SalaryAccount();
        obj3.deposit(20000);
        obj3.withdraw(5000);

    }
}